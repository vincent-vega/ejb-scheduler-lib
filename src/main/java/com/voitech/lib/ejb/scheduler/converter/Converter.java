package com.voitech.lib.ejb.scheduler.converter;

import com.voitech.lib.ejb.scheduler.serializer.Serializer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author wkoszycki
 */
public class Converter {

    @Resource(lookup = "java:app/AppName")
    private String appName;
    private static final Logger LOG = Logger.getLogger(Converter.class.getName());
    private static final String DIR = "/tmp";

    @PostConstruct
    private void init() {
        try {
            makeDir();
            makeFile();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void serialize(Map<String, ScheduledTask> map) throws IOException {
        List<ScheduledTaskDTO> list = new ArrayList<ScheduledTaskDTO>();

        for (Map.Entry<String, ScheduledTask> entry : map.entrySet()) {
            ScheduledTask task = entry.getValue();
            if (task.isSerialize()) {
                list.add(entry.getValue().convertToDTO());
            }

        }
        Serializer.serialize(new DtoWrapper(list), FileUtils.openOutputStream(new File(DIR + appName)));
    }

    public Map<String, ScheduledTask> deserialize() throws IOException, NoSuchMethodException {

        byte[] bytes = FileUtils.readFileToByteArray(new File(DIR + appName));
        Map<String, ScheduledTask> map = new HashMap<String, ScheduledTask>();
        if (bytes.length > 0) {
            DtoWrapper wrapper = (DtoWrapper) Serializer.deserialize(bytes);

            for (ScheduledTaskDTO dto : wrapper.getTasks()) {
                map.put(dto.getTimername(), new ScheduledTask(dto));
            }
        }
        return map;
    }

    private void makeDir() {
        File dir = new File(DIR);
        if (!dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    private void makeFile() throws IOException {
        File file = new File(DIR + appName);
        if (!file.isFile()) {
            file.createNewFile();
        }
    }
}
