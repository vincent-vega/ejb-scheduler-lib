/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voitech.lib.ejb.scheduler.converter;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author wkoszycki
 */
 public class DtoWrapper implements Serializable {

    private List<ScheduledTaskDTO> tasks;

     DtoWrapper(List<ScheduledTaskDTO> tasks) {
        this.tasks = tasks;
    }

    public Collection<ScheduledTaskDTO> getTasks() {
        return tasks;
    }
}