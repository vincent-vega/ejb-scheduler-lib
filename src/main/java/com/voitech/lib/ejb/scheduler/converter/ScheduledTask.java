package com.voitech.lib.ejb.scheduler.converter;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ScheduleExpression;
import javax.ejb.TimerConfig;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 *
 * @author wkoszycki
 */
public class ScheduledTask implements Serializable {

    private static final Logger LOG = Logger.getLogger(ScheduledTask.class.getName());
    private String timername;
    private Method method;
    private Class<?> targetClass;
    private Object target;
    private Collection<Object> args = new ArrayList<Object>();
    private ScheduleExpression expression;
    private TimerConfig timerConfig;
    private String beanname;
    private boolean ejb;
    private boolean paused;
    private boolean serialize;

    /**
     * Create new ScheduledTask Object
     *
     * @param timername name of timer
     */
    public ScheduledTask(String timername, boolean serialize) {
        this.timername = timername;
        this.serialize = serialize;
        this.timerConfig = new TimerConfig(timername, false);
    }

     ScheduledTask(ScheduledTaskDTO dto) throws NoSuchMethodException {
        this.timername = dto.getTimername();
        this.timerConfig = new TimerConfig(timername, false);
        this.expression = dto.getExpression();
        this.beanname = dto.getBeanname();
        this.ejb = dto.isEjb();
        this.paused = dto.isPaused();
        this.args = dto.getArgs();
        this.targetClass = convertClassFromString(dto.getTargetClass())[0];
        this.serialize = false;
        initializeTarget();
        this.method = this.target.getClass().getMethod(dto.getMethod(), convertClassFromString(dto.getMethodParams()));

    }

    public void run() {
        try {
            if (target == null) {
                initializeTarget();
            }
            this.method.invoke(target, args.toArray());
        } catch (Throwable ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }

    }

    private void initializeTarget() {
        try {

            if (isEjb()) {
                Context context = new InitialContext();
                target = context.lookup(beanname);
            } else {
                target = targetClass.newInstance();
            }

        } catch (Throwable ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @return Current state of timer
     */
    public boolean isPaused() {
        return paused;
    }

    public boolean isSerialize() {
        return serialize;
    }

    
    
    public void pause() {
        this.paused = true; 
    }

    public void play() {
        this.paused = false;
    }

    /**
     *
     * @return
     */
    public String getBeanname() {
        return beanname;
    }

    /**
     * This method indicates that target object is ejb
     *
     * @param beanname
     * @return ScheduledTask object
     */
    public ScheduledTask beanName(String beanname) {
        this.beanname = beanname;
        this.ejb = true;
        return this;
    }

    boolean isEjb() {
        return ejb;
    }

    /**
     *
     * @param name name of method
     * @param params method parameters
     * @return this
     * @throws NoSuchMethodException if target class doesn't have such method
     */
    public ScheduledTask method(String name, Class<?>... params) throws NoSuchMethodException {
        if (target == null) {
            initializeTarget();
        }

        this.method = this.target.getClass().getMethod(name, params);
        return this;
    }

    /**
     * Indicates state of timer default is false
     *
     * @param paused flag
     * @return this
     */
    public ScheduledTask paused(boolean paused) {
        this.paused = paused;
        return this;
    }

    /**
     *
     * @param target target class to run method
     * @return this
     */
    public ScheduledTask target(Class<?> target) {
        this.targetClass = target;
        return this;
    }

    /**
     *
     * @param beanname target bean to run method
     * @return
     */
    public ScheduledTask target(String beanname) {
        this.beanname = beanname;
        return this;
    }

    /**
     *
     * @param arg parameter of method must be provided with correct order
     * @return this
     */
    public ScheduledTask addArgument(Object arg) {
        this.args.add(arg);
        return this;
    }

    /**
     *
     * @param expression schedule expression
     * @return this
     */
    public ScheduledTask expression(ScheduleExpression expression) {
        this.expression = expression;
        return this;
    }

    /**
     *
     * @return schedule expression
     */
    public ScheduleExpression getExpression() {
        return expression;
    }

    /**
     *
     * @return name of timer
     */
    public String getTimername() {
        return timername;
    }

    /**
     *
     * @return method of timer
     */
    public Method getMethod() {
        return method;
    }

    public TimerConfig getTimerConfig() {
        return timerConfig;
    }

    private Class<?>[] convertClassFromString(String... params) {

        int size = params.length;

        Class<?>[] arr = new Class<?>[size];

        try {
            for (int i = 0; i < size; i++) {
                arr[i] = Class.forName(params[i]);
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }
        return arr;
    }

     ScheduledTaskDTO convertToDTO() {
        Class<?>[] parameterTypes = this.method.getParameterTypes();

        String[] paramArray = new String[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            paramArray[i] = parameterTypes[i].getName();
        }

        return new ScheduledTaskDTO(this.timername, this.method.getName(), paramArray, this.targetClass.getName(), args, this.expression, this.beanname, this.ejb, this.paused);
    }
}
