/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voitech.lib.ejb.scheduler.converter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.ejb.ScheduleExpression;

/**
 *
 * @author wkoszycki
 */
 class ScheduledTaskDTO implements Serializable {

    private String timername;
    private String method;
    private String[] methodParams;
    private String targetClass;
    private Collection<Object> args;
    private ScheduleExpression expression;
    private String beanname;
    private boolean ejb;
    private boolean paused;

    public ScheduledTaskDTO(String timername, String method,String[] methodParams, String targetClass,Collection<Object> args, ScheduleExpression expression, String beanname, boolean ejb, boolean paused) {
        this.timername = timername;
        this.method = method;
        this.targetClass = targetClass;
        this.expression = expression;
        this.beanname = beanname;
        this.ejb = ejb;
        this.paused = paused;
        this.methodParams = methodParams;
        this.args = args;
    }

    public String getTimername() {
        return timername;
    }

    public String getMethod() {
        return method;
    }

    public String getTargetClass() {
        return targetClass;
    }

    public Collection<Object> getArgs() {
        return args;
    }

    public ScheduleExpression getExpression() {
        return expression;
    }

    public String getBeanname() {
        return beanname;
    }

    public boolean isEjb() {
        return ejb;
    }

    public boolean isPaused() {
        return paused;
    }

    public String[] getMethodParams() {
        return methodParams;
    }
    
    
}
